# README #

SDRSharp.Autotrack: behaves like a software LNB lock. Still a long way to go.

### What is Autotrack? ###

* Autotrack is an SDRSharp plugin used to lock and track a particular modulated signal (BPSK, QPSK,...) in a particular bandwidth (see below).
It locks by using an internal Costas PLL and it tracks by constantly adjusting the tuned frequency of the SDR.

### Note ###
* The binaries are offered as is. They will stop working at the end of January 2021.
* Backup your current SDRSharp installation before installing!
* The compatibility with SDRSparp.ScytaleC plugin was not verified.
* Does not correctly reports RF for a universal LNB.

### Requirements? ###

1. You need a signal that is unique in the bandwidth. The bandwidth is 4kHz, but it seem usable down to about 2kHz.
2. You need to actually tune your SDR to the frequency you wish to lock onto.
3. All other SDRSharp plugin requirements.
4. Set your radio to RAW as I set the PLL CF to 0.0Hz

### What does it offer? ###

* Ability to lock and track a modulated signal in given bandwidth, e.g., 2KHz off CF.
* Ability to log the frequency shifts over time.
* Ability to log the signal strength over time.
* An easy calculator for the frequency translation between IF and the transmitter.

### What does it not offer? ###

* Does not run in the background. Silent mode not implemented.

### How to use? ###

* Set IF min. If this would be a standard satellite receiver this would be the lower limit of the 950 to 2150 MHz range.
* Set LNB type. Currently Single or Universal.
* Set LO min/max/switch. These are the LO numbers of the LNB.
* Tracking CIF. Autotrack sets both the IF and Central IF of the SDR to keep the lock on the signal and the signal in the same spot on the FFT Display.
* Tracking IF. Autotrack sets only the IF of the SDR to keep the lock on the signal.
* Silent. Not implemented. Run in the background. Keep lock on frequency and same relative FFT Display position. Acts like a "software" LNB lock. User is free to use the radio taking advantage of the background lock.
* Log to file. Logs to a comma delimited log, one file a day. The format is: dd-HH:mm:ss,dBFS,IF,CIF,RF,Correctionfactor 
* RF. Partially implemented.
* IF. 


### Final note ###

* Use the issue tracker for opinions, issues, additional requirements...
* Backup your current SDRSharp installation before installing!

### Updates ###

v1.0.0.1 alpha
* Added support for Universal LNB.
* Changed output format to comma-delimited instead of tab-delimited.
